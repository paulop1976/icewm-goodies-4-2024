# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-16 13:19+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: edit_antix_startup_file:10
msgid "antiX-startup GUI"
msgstr ""

#: edit_antix_startup_file:11
msgid "Startup ("
msgstr ""

#: edit_antix_startup_file:12
msgid "Add application from a list"
msgstr ""

#: edit_antix_startup_file:13
msgid "Add a (manual) command"
msgstr ""

#: edit_antix_startup_file:14
msgid "Remove an application"
msgstr ""

#: edit_antix_startup_file:15
#, sh-format
msgid ""
"The line was added to $startupfile. It will start automatically the next "
"time you start  antiX"
msgstr ""

#: edit_antix_startup_file:16
msgid "Enter command to be added to the startup file"
msgstr ""

#: edit_antix_startup_file:17
msgid ""
"Enter command you want to run at antiX's startup. Note: an ampersand/& will "
"automatically be appended to the end of the command"
msgstr ""

#: edit_antix_startup_file:19
msgid "Remove"
msgstr ""

#: edit_antix_startup_file:22
#, sh-format
msgid "\\n $add_remove_text $startupfile):"
msgstr ""
