��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  b  �  $     *   5  <   `  �   �  	   !     +  	   D  m   N  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: aer, 2023
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Einen (manuellen) Befehl hinzufügen Eine Anwendung von einer Liste hinzufügen Befehl eingeben, der zur Startdatei hinzugefügt werden soll Befehl eingeben, der beim Start von antiX ausführt werden soll. Hinweis: Ein & wird automatisch an das Ende des Befehls angehängt Entfernen Eine Anwendung entfernen Startup ( Die Zeile wurde zu $startupfile hinzugefügt. Sie wird beim nächsten Start von antiX automatisch ausgeführt \n $add_remove_text $startupfile): antiX-Systemstart-Einstellungen 