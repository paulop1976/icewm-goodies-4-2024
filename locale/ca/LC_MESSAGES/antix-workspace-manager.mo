��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  ~  �     &     -     4  E   @  >   �     �     �     �  )   �  F     �   [     �  )   
  /   4  $   d  *   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Aplica Enrere Canvia Noms Cliqueu Aplica un cop s'hagin entrat els noms dels espais de treball. Cliqueu Fet quan s'hagi definit el nombre d'Espais de Treball. Fet  Entreu els noms desitjats: Surt Definiu el nombre d'escriptoris desitjat: Per canviar els noms dels Espais de Treball cliqueu sobre Canvia Noms. Useu la roda del ratolí o cliqueu a les fletxes. Podeu entrar un número amb el teclat, seguit de la tecla Retorn. El canvi es produeix al moment. Número d'Espai de Treball Commutador d'espais de treball aWCS antiX Canviador de noms d'espai de treball aWCS antiX Commutador d'espais de treball antiX Canviador de noms d'espai de treball antiX 