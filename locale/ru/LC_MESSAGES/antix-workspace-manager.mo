��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     �  
   �     �  v   �  �   P     �  +   �       I   &  s   p    �  4   �  N   1	  [   �	  I   �	  V   &
                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Andrei Stepanov, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Применить Назад Сменить имена Нажмите «Применить» по вводу желаемых имён рабочих пространств. Нажмите кнопку Готово, когда будет установлено желаемое число рабочих мест. Готово Введите желаемые имена: Оставить Установите желаемое число рабочих мест: Для смены имён рабочих пространств щёлкните по Изменить имена. Используйте колесо мыши или щелкайте по стрелкам. Вы также можете ввести число с клавиатуры и затем нажать клавишу Enter. Изменение произойдёт немедленно. Номер рабочего пространства Переключатель счётчика рабочих мест aWCS antiX Средство смены имени рабочего пространства aWCS antiX Переключатель счётчика рабочих мест antiX Средство смены имени рабочего пространства antiX 