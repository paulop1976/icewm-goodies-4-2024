��          �      \      �     �     �     �  !     .   5  0   d     �     �  D   �      �          '  7   .     f           �     �     �     �  �  �     �     �  )     3   /  F   c  H   �     �  
     c     2   u     �     �  [   �  *   )  
   T  1   _     �     �     �     	                         
                                                                     ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Move No changes were made!\nTIP: you can always try the Advanced buttton. Personal Menu Ultra Fast Manager REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop UNDO LAST STEP!undo:FBTN Warning You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-19 14:12+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2023
Language-Team: Hebrew (Israel) (https://app.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 הוספת סמל!add:FBTN מתקדם!help-hint:FBTN הוספת סמל היישום הנבחר נא לבחור מה לעשות עם הסמל $EXEC לחיצה כפולה על יישום תעביר את הסמל שלו: לחיצה כפולה על יישום כלשהו תסיר את הסמל: עזרה!help:FBTN העברה לא בוצעו שינויים!\nעצה: תמיד אפשר לנסות את הכפתור מתקדם. מנהל תפריט אישי מהיר במיוחד הסרת סמל!remove:FBTN הסרה הסקריפט הזה מיועד להרצה בשולחן עבודה מסוג IceWM בלבד ביטול הצעד האחרון!undo:FBTN אזהרה מופעל אצלך שולחן העבודה IceWM בקובץ יש משהו הקובץ ריק שום דבר לא נבחר 