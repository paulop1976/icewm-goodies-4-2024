# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-02-09 01:17+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: icewm-manager-gui.sh:6 icewm-manager-gui.sh:7
msgid "antiX Control Centre"
msgstr ""

#: icewm-manager-gui.sh:12 icewm-manager-gui.sh:1332
msgid "Basic IceWM Settings Manager"
msgstr ""

#: icewm-manager-gui.sh:13
msgid ""
"Select what you want to change from the options below. If an option fails to "
"work, please try clicking it again."
msgstr ""

#: icewm-manager-gui.sh:14
msgid "If an option fails to work, please try clicking it again."
msgstr ""

#: icewm-manager-gui.sh:15
msgid "Clock"
msgstr ""

#: icewm-manager-gui.sh:16
msgid "Show clock on/off"
msgstr ""

#: icewm-manager-gui.sh:17
msgid "12h/24h time"
msgstr ""

#: icewm-manager-gui.sh:18
msgid "Show seconds on clock on/off"
msgstr ""

#: icewm-manager-gui.sh:19
msgid "Show clock in LED on/off"
msgstr ""

#: icewm-manager-gui.sh:20
msgid "System information shown in System Tray"
msgstr ""

#: icewm-manager-gui.sh:21
msgid "Show Network information on System Tray on/off"
msgstr ""

#: icewm-manager-gui.sh:22
msgid "Show RAM information on System Tray on/off"
msgstr ""

#: icewm-manager-gui.sh:23
msgid "Show CPU, RAM and SWAP  information on System Tray on/off"
msgstr ""

#: icewm-manager-gui.sh:24
msgid "Window Management"
msgstr ""

#: icewm-manager-gui.sh:25
msgid "Show Application icon on window title bar on/off"
msgstr ""

#: icewm-manager-gui.sh:26
msgid "Centre new large windows on/off"
msgstr ""

#: icewm-manager-gui.sh:27
msgid "Show indication of position when moving windows  on/off"
msgstr ""

#: icewm-manager-gui.sh:28
msgid "Click a Window to memorize its position"
msgstr ""

#: icewm-manager-gui.sh:29
msgid "Auto Start"
msgstr ""

#: icewm-manager-gui.sh:30
msgid "Add or Remove an application from IceWM start file"
msgstr ""

#: icewm-manager-gui.sh:31
msgid "Toolbar generic settings"
msgstr ""

#: icewm-manager-gui.sh:32
msgid "Auto-hide toolbar on/off"
msgstr ""

#: icewm-manager-gui.sh:33
msgid "Double height toolbar on/off"
msgstr ""

#: icewm-manager-gui.sh:34
msgid "Toolbar on top/bottom of screen"
msgstr ""

#: icewm-manager-gui.sh:35
msgid "Show window names on toolbar on/off"
msgstr ""

#: icewm-manager-gui.sh:36
msgid "Manage quick launch toolbar icons"
msgstr ""

#: icewm-manager-gui.sh:37
msgid "'Show Desktop' icon on/off"
msgstr ""

#: icewm-manager-gui.sh:38
msgid "Workspace indicator on/off"
msgstr ""

#: icewm-manager-gui.sh:39
msgid "Workspace Manager"
msgstr ""

#: icewm-manager-gui.sh:40
msgid "Menu"
msgstr ""

#: icewm-manager-gui.sh:41
msgid "Manage Personal Menu entries"
msgstr ""

#: icewm-manager-gui.sh:42
msgid "Look and feel"
msgstr ""

#: icewm-manager-gui.sh:43
msgid "Wallpaper"
msgstr ""

#: icewm-manager-gui.sh:44
msgid "Appearance"
msgstr ""

#: icewm-manager-gui.sh:45
msgid "Select IceWM Theme"
msgstr ""

#: icewm-manager-gui.sh:46
msgid "Selected app icon to place on the (zzz) Desktop"
msgstr ""

#: icewm-manager-gui.sh:47
msgid ""
"Do you want to add or to remove an entry to IceWm's startup? Current startup "
"file"
msgstr ""

#: icewm-manager-gui.sh:48
#, sh-format
msgid ""
"The line was added to the $HOME/.icewm/startup file. It will start "
"automatically the next time you start  antiX using the IceWM desktop"
msgstr ""

#: icewm-manager-gui.sh:49
msgid "Enter command to be added to IceWM's startup file"
msgstr ""

#: icewm-manager-gui.sh:50
msgid ""
"Enter command you want to run at IceWM's startup. Note: an ampersand/& will "
"automatically be appended to the end of the command"
msgstr ""

#: icewm-manager-gui.sh:52
msgid "Remove"
msgstr ""

#: icewm-manager-gui.sh:53
#, sh-format
msgid ""
"The line was removed from $HOME/.icewm/startup file. The related application "
"will no longer start automatically the next time you start  antiX using the "
"IceWM desktop"
msgstr ""

#: icewm-manager-gui.sh:54
msgid "IceWM Themes"
msgstr ""

#: icewm-manager-gui.sh:55
msgid "Double click its name to select a new Theme:"
msgstr ""

#: icewm-manager-gui.sh:56
msgid "The current IceWM Theme is "
msgstr ""

#: icewm-manager-gui.sh:57
msgid "Manage keyboard shortcuts"
msgstr ""

#: icewm-manager-gui.sh:58
msgid "Warning"
msgstr ""

#: icewm-manager-gui.sh:59
msgid "This script is meant to be run only in an IceWM desktop"
msgstr ""

#: icewm-manager-gui.sh:60
msgid "Reset to default contents"
msgstr ""

#: icewm-manager-gui.sh:61
msgid "Show only used network devices on/off"
msgstr ""

#: icewm-manager-gui.sh:62
msgid "Collapse Toolbar button on/off"
msgstr ""

#: icewm-manager-gui.sh:63
msgid "Show Weather icon on/off"
msgstr ""

#: icewm-manager-gui.sh:64
msgid "Manually edit config files"
msgstr ""

#: icewm-manager-gui.sh:65
msgid "Personal sub-menu on/off"
msgstr ""

#: icewm-manager-gui.sh:66
msgid "Set time and date"
msgstr ""

#: icewm-manager-gui.sh:67
msgid "Personal entries on the first layer of menu on/off"
msgstr ""

#: icewm-manager-gui.sh:68
msgid "Run menu entry on/off"
msgstr ""

#: icewm-manager-gui.sh:69
msgid "Show/hide Conky resource indicator on the desktop"
msgstr ""

#: icewm-manager-gui.sh:70
msgid "Icewm Settings in menu on/off"
msgstr ""

#: icewm-manager-gui.sh:71
msgid "Xkill menu entry on/off"
msgstr ""

#: icewm-manager-gui.sh:72
msgid "IceWM Toolbar width and horizontal position"
msgstr ""

#: icewm-manager-gui.sh:73
msgid "Toolbar"
msgstr ""

#: icewm-manager-gui.sh:74
msgid "System tray"
msgstr ""

#: icewm-manager-gui.sh:75
msgid "Toggle Compositor on/off"
msgstr ""

#: icewm-manager-gui.sh:76
msgid ""
"This button adds/removes an icon to the toolbar that displays the weather on "
"a text window, using the wttr.in web service, that tries to auto-detect your "
"current location"
msgstr ""

#: icewm-manager-gui.sh:77
#, sh-format
msgid ""
"You can't directly manage IceWM's toolbar height. You can choose double "
"height/normal height, but, other than that, the height of the toolbar is set "
"by the Theme that is being used (that you can change by clicking the theme "
"button on the '$look_feel' tab)"
msgstr ""

#: icewm-manager-gui.sh:78
msgid ""
"By default, 2 network information squares are shown in the system tray, one "
"for ethernet connections and another for wi-fi connections. This option "
"allows to save toolbar space and display just the Internet connection that "
"is being used (making the information easier to read), or restore the "
"default setting, showing both squares. When no internet connection is "
"detected, no network information square will be displayed in the toolbar, if "
"this option is selected (but when network is available, the square will "
"appear again)"
msgstr ""

#: icewm-manager-gui.sh:79
msgid ""
"This feature, when enabled, allows the toolbar to shrink to the size of a "
"small arrow, on the corner of the screen. When you click that arrow, or "
"press the 'Windows'/Super key, the toolbar is restored back to it's normal "
"size. This allows better use of screen space, particularly in very small "
"screens"
msgstr ""

#: icewm-manager-gui.sh:80
msgid ""
"This allows users to manage virtual workspaces and change the name of "
"existing workspaces permanently and/or change the number of workspaces "
"available for the current session. If you don't use virtual desktops, you "
"can disable this indicator and save a bit of toolbar space"
msgstr ""

#: icewm-manager-gui.sh:81
msgid ""
"This allows users to hide/show the virtual desktop indicator in the toolbar. "
"Even if the virtual desktop indicator is not shown, you can still switch "
"between virtual desktops, using keyboard shortcuts or any other way"
msgstr ""

#: icewm-manager-gui.sh:82
msgid ""
"This allows user to hide/show the toolbar icon (by default it's right next "
"to the menu button, on the top left). When clicked this icon toggles between "
"showing the currently non minimized windows or showing the desktop background"
msgstr ""

#: icewm-manager-gui.sh:83
msgid ""
"This launches the window that allows users to add, remove or move quick-"
"launch icons to the toolbar. Please note that you can't drag and drop "
"toolbar quick-launch icons (nor move them around). You can also add/remove "
"quick-launch icons using app-select (the app launcher/app finder available "
"in the menu)"
msgstr ""

#: icewm-manager-gui.sh:84
msgid ""
"This allows users to toggle having both the icons and names of currently "
"running windows on the taskbar section of the the toolbar, or having just "
"their icons. Displaying only icons does save toolbar space, and is used by "
"default in many Operating Systems... but it can lead to confusion if you "
"have multiple windows of the same app, like a word processor, open"
msgstr ""

#: icewm-manager-gui.sh:85
msgid ""
"This opens a window that allows you to select the percentage of horizontal "
"space the toolbar will use and also it's horizontal position (note that it "
"is always shown by default on the window as being in the Center, but it can "
"also be on the Left or on the Right). Changes only produce effects after you "
"click the 'Ok' button"
msgstr ""

#: icewm-manager-gui.sh:86
msgid ""
"This allows you to have the toolbar on the bottom of the screen (the default "
"place) or on the top of the screen. After moving the toolbar to the top, you "
"should restart your applications, so their top section is not hidden by the "
"toolbar. Please note that IceWM does not allow the toolbar to be placed "
"vertically on the sides of the screen"
msgstr ""

#: icewm-manager-gui.sh:87
msgid ""
"This toggles between always showing the toolbar (the default setting) or "
"hiding it when it's not being used. To see the toolbar again simply move the "
"mouse pointer to the place where it's hiding (by the default, the bottom of "
"the screen) and it will appear, or press the Windows/Super key. When not "
"being used, the toolbar will then automatically hide. Please note that after "
"you toggle this selection you should resize or restart your apps, so they "
"recognize the available screen space was changed"
msgstr ""

#: icewm-manager-gui.sh:88
msgid ""
"This allows to display (the default) or hide the square(s) that show network "
"information on the system tray. Note: if you double left click this "
"information squares, by default, you summon the Conman network manager's "
"window."
msgstr ""

#: icewm-manager-gui.sh:89
msgid ""
"This allows to display (the default) or hide the square that displays "
"detailed information about the use of your RAM on the system tray. (Note: "
"that general information about your RAM use is already displayed in another "
"information square by default)"
msgstr ""

#: icewm-manager-gui.sh:90
msgid ""
"This allows to display on the system tray (the default) or hide the square "
"that displays detailed information about the use of CPU, RAM, Swap, CPU "
"Temperature and frequency. Note: every time you double left click this "
"information square, by default, you summon the Task Manager's window"
msgstr ""

#: icewm-manager-gui.sh:91
msgid ""
"This allows to show (the default) or hide the clock shown on the toolbar. "
"Note: when you double left click the clock, by default, you summon the antiX "
"Calendar's window. When you right click the clock, you can select the way "
"the time is shown, including if the date is to be displayed, for example"
msgstr ""

#: icewm-manager-gui.sh:92
msgid ""
"This toggles the toolbar clock mode between displaying time in 12h or 24h. "
"Note that this does not change the way time is displayed system wide, only "
"on the toolbar clock"
msgstr ""

#: icewm-manager-gui.sh:93
msgid ""
"Show or hide seconds on the toolbar clock. Having seconds always displayed "
"can be a bit distracting, but, on the other hand it can also be helpful as "
"an easy way to measure small amounts of time. Note that this does not change "
"the way time is displayed system wide, only on the toolbar clock"
msgstr ""

#: icewm-manager-gui.sh:94
msgid ""
"This allows to reset clock to the original settings, related to showing "
"seconds, date, 12/24h format. Note: this does not affect using or not the "
"LCD type of clock display"
msgstr ""

#: icewm-manager-gui.sh:95
msgid ""
"This enables or disables (the default) using special numbers, created to "
"usually mimic old LCD clock displays, to show the time. Note: the aspect of "
"the LCD's can vary according to the IceWM theme being used. Also, LCD can't "
"display some data, like, for example, the full date"
msgstr ""

#: icewm-manager-gui.sh:96
msgid ""
"This allows you to run the same window that antiX Control Centre uses to set "
"time and date or select your time-zone. If asked to, you'll have to enter "
"your password. IMPORTANT NOTE: Changing time and date, of course, affects "
"the way all window managers, not just IceWM, show the time"
msgstr ""

#: icewm-manager-gui.sh:97
msgid ""
"This allows to show or hide applications icons on the windows on the left "
"side of their title bars. Some IceWM themes enable or disable this setting "
"by default. Here you can force the icon to always be displayed or always be "
"hidden"
msgstr ""

#: icewm-manager-gui.sh:98
msgid ""
"In IceWM, by default windows open on random places, in order not to cover "
"completly previous windows. The option allows users to select to always "
"display large windows on the center of the screen or to use the default "
"behaviour"
msgstr ""

#: icewm-manager-gui.sh:99
msgid ""
"By default, when the user moves a window, IceWM displays it's exact "
"coordinates on the bottom center of the screen (to help fine tune the "
"position and use it later). Using this option the user can disable or enable "
"that behaviour"
msgstr ""

#: icewm-manager-gui.sh:100
msgid ""
"Clicking this option will instantly start the GUI program that allows IceWM "
"to memorise the selected window's position. The mouse pointer will change to "
"a '+'. Click anywhere on the window you want to 'memorize' and the GUI's "
"main window will appear, so you can confirm the selection (by clicking "
"'Ok'), cancel the action or select a different window to 'memorize'."
msgstr ""

#: icewm-manager-gui.sh:101
msgid ""
"The Personal Menu is where you can Pin your favourite or most used "
"applications, for quicker access. This option opens the window for the GUI "
"program that allows you to add, remove or move application from that list. "
"Note: app-select (the app launcher/app finder available in the menu) also "
"allows users to add or remove applications to/from the Personal Menu."
msgstr ""

#: icewm-manager-gui.sh:102
msgid ""
"The Personal Menu is, by default, showed as a sub-menu of the antiX Menu. "
"This option allows you to completely disable that sub-menu, either because "
"you don't use it and don't want to waste menu space on it or because you "
"want to directly display your 'personal' applications on the first layer of "
"the antiX menu (you can manage that with the button below this one)"
msgstr ""

#: icewm-manager-gui.sh:103
msgid ""
"The Personal Menu is, by default, showed as a sub-menu of the antiX Menu. "
"This option allows you to display your 'Personal' apps directly on the first "
"layer of the menu, for quicker access. Note that, to save menu space if you "
"enable this feature, it's advisable to use the option above this one to make "
"sure the 'Personal sub-menu' is not shown. Even if your 'Personal' apps are "
"shown on the first layer of the menu, editing your Personal menu list can "
"never damage your antiX menu.  WARNING: please take care not to display your "
"'Personal' entries on the first layer of the menu if that makes the menu "
"taller than the available screen space, because that makes the menu a bit "
"harder to use, since it's top will be off screen"
msgstr ""

#: icewm-manager-gui.sh:104
msgid ""
"By default, the antiX Menu always shows, near the bottom, an entry that "
"allows users to force close windows (handy if the application is frozen). If "
"you have no need for this option, you can disable it, or enable it again any "
"time you want. When you click this antiX Menu entry, the mouse pointer will "
"turn into an 'x'. Any window you click next will be automatically closed. If "
"you don't want to close any window, press the 'Esc' key and the mouse "
"pointer will be back to normal"
msgstr ""

#: icewm-manager-gui.sh:105
msgid ""
"By default, the antiX Menu always shows, near the bottom, an entry that "
"shows a window that allows users to run any command, or start any "
"application by typing it's executable's name. If you have no need for this "
"option, you can disable it, or enable it again any time you want"
msgstr ""

#: icewm-manager-gui.sh:106
msgid ""
"By default, the antix Menu always shows, near the bottom, a sub-menu that "
"allows access to the manual edition of several IceWM settings. If you have "
"no need for this option (since the most used options are already managed "
"using this window), you can disable it (to make the menu more streamlined "
"and simpler to use), and enable it again any time you want. Note: by "
"default, this menu entry allows access to the IceWM 'Themes' menu, that "
"allows to change Themes instantly. When you select to change any setting in "
"the 'Preferences' sub-menu, you have to always click the last entry, that "
"saves the changes, and usually, you'll also have to restart IceWM, so the "
"changes produce effects"
msgstr ""

#: icewm-manager-gui.sh:107
msgid ""
"This opens the window that allows you to select the background of the "
"desktop. It can be any image or even a simple colour."
msgstr ""

#: icewm-manager-gui.sh:108
msgid ""
"This opens LXappearance's window, that allows you to select the GTK theme "
"(that manages the interior contents of some windows), the icon set, the way "
"the mouse pointer looks, etc. Note: selecting a light or dark GTK theme  can "
"be used in conjunction with a IceWM Theme (that you can select by clicking "
"the button below this one), in order to have a 'light/dark mode'"
msgstr ""

#: icewm-manager-gui.sh:109
msgid ""
"This enables you to show or hide the conky (the system information that is, "
"by default displayed on the top right corner of your desktop's background. "
"Note that conky can be (manually) edited to look almost any way you want, "
"and to display several things, including the Weather, Rss News feeds, etc- "
"acting almost like a group of widgets"
msgstr ""

#: icewm-manager-gui.sh:110
msgid ""
"This opens a window that allows you to add, remove or edit keyboard "
"shortcuts used in IceWM"
msgstr ""

#: icewm-manager-gui.sh:111
msgid ""
"This opens an app-select window (that can take a few moments to load). When "
"you select any application on that window it's icon will instantly be added "
"to the desktop (if the currently used desktop allows using desktop icons) "
"Please note that this also works on rox... desktops, but in a quirky way"
msgstr ""

#: icewm-manager-gui.sh:112
msgid ""
"This opens a window that allows you to select a IceWM theme. Themes manage "
"the way the toolbar, the menu and the window decorations (this means the "
"window title bar and it's buttons and window frame look). The selection "
"window remains open until you close it, allowing you to select the theme you "
"like best. Note: selecting a light or dark IceWM theme can be used in "
"conjunction with the Appearance window (available by clicking the button "
"above this one), in order to have a 'light/dark mode'"
msgstr ""

#: icewm-manager-gui.sh:113
msgid ""
"Enable or disables using the video compositor. Enabling video composition "
"allows having visual effects, like fading in/fading out windows, shadows, "
"etc. It can also solve some video problems. Using a compositor, however does "
"use a bit of system resources. If you have a weak computer, not using a "
"compositor can help it run a bit faster"
msgstr ""

#: icewm-manager-gui.sh:114
msgid ""
"This opens a window that allows you to add an application (selected using "
"app-select, that can that a few moments to load), add a command (typed by "
"you), or remove any line, to/from IceWM's 'startup' configuration file. Any "
"application or command that's listed on this file is run every time the "
"Window Manager starts. NOTE: the Window Manager configuration file is not "
"the same as antiX's desktop-session startup file. Both of these files manage "
"the commands that are run when antiX starts. The desktop-session startup "
"file affects all window managers, not just IceWM, and it has it's own GUI, "
"available in antiX Menu > Applications > antiX"
msgstr ""

#: icewm-manager-gui.sh:115
msgid "Toggle indicator of keyboard layout on/off"
msgstr ""

#: icewm-manager-gui.sh:116
msgid ""
"Permanently enable (default setting) or disable the keyboard language "
"indicator flag (it's called 'fbxkb'), that is displayed in the system tray. "
"If you use only one keyboard layout, you probably have really no need for "
"it, and it just stays there, using system resources. WARNING: changing this "
"setting produces effects on all Window Managers, not just IceWM"
msgstr ""

#: icewm-manager-gui.sh:117
msgid "Recent Files"
msgstr ""

#: icewm-manager-gui.sh:118
msgid "Select number of recent files (displayed in the menu)"
msgstr ""

#: icewm-manager-gui.sh:119
msgid "Clear antiX's recent files history"
msgstr ""

#: icewm-manager-gui.sh:120
msgid ""
"This displays a window that allows you to select exactly how many recent "
"files should be displayed in the antiX IceWM Menu- In that window, select "
"exactly the number of files you want to see and click the 'Ok' button. If "
"you cancel or close that window, the number of recent files to display will "
"be '0' (but the list of recent files won't be deleted, you can later reset "
"it to any number you want)"
msgstr ""

#: icewm-manager-gui.sh:121
msgid ""
"This displays a window that allows you to completely delete the list of "
"recent files that is displayed by antiX. A confirmation window will appear. "
"If you click the 'Delete' button, a notification window will inform that the "
"procedure was finished. WARNING: This is done system wide, not just in "
"IceWM's Recent files menu"
msgstr ""

#: icewm-manager-gui.sh:558
msgid "Add application from a list"
msgstr ""

#: icewm-manager-gui.sh:559
msgid "Add a (manual) command"
msgstr ""

#: icewm-manager-gui.sh:560
msgid "Remove a line"
msgstr ""

#: icewm-manager-gui.sh:561
msgid "Edit IceWM startup file"
msgstr ""

#: icewm-manager-gui.sh:562
msgid "Command to be added to IceWM startup file"
msgstr ""

#: icewm-manager-gui.sh:566
#, sh-format
msgid "\\n $add_remove_text $HOME/.icewm/startup):"
msgstr ""

#: icewm-manager-gui.sh:838
msgid "IceWM Toolbar Width"
msgstr ""

#: icewm-manager-gui.sh:839
msgid "Position"
msgstr ""

#: icewm-manager-gui.sh:840
msgid "center"
msgstr ""

#: icewm-manager-gui.sh:841
msgid "left"
msgstr ""

#: icewm-manager-gui.sh:842
msgid "right"
msgstr ""

#: icewm-manager-gui.sh:920
msgid ""
"Please restart any open window so the GTK theme change can be applied to it "
"\\n This window is already displayed in the currently selected GTK theme. "
"\\n Certain apps (like Firefox, and Geany) have to be configured to use it's "
"own Dark/Light Modes or Themes."
msgstr ""

#: icewm-manager-gui.sh:920
msgid "Global Theme change"
msgstr ""

#: icewm-manager-gui.sh:1031
msgid "(No tooltip for now, sorry.)"
msgstr ""
